# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    alpha = False
    digit = False
    upper = False
    lower = False
    special = False
    length = False

    for char in password:
        if char.isalpha():
            alpha = True
            if char.isupper():
                upper = True
            elif char.islower():
                lower = True
        if char == "$" or char == "!" or char == "@":
            special = True
        if char.isdigit():
            digit = True
        if 6 <= len(password) <= 12:
            length = True
    return (
        length
        and alpha
        and upper
        and lower
        and special
        and digit
    )

print(check_password("Vh6kSaaaaaaaaaa612!"))
