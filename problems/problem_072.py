# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.


class PersonalTaste:
    def __init__(self, name, love_food, hate_food):
        self.name = name
        self.love_food = love_food
        self.hate_food = hate_food

    def taste(self, food):
        if food in self.love_food:
            return print("I love that food!")
        elif food in self.hate_food:
            return print("Not the biggest fan...")
        elif food not in self.hate_food or self.love_food:
            return None

personal_taste = PersonalTaste("Mommy", ["Plants", "Beans"], ["Dairy", "Meat"])

personal_taste.taste("Plants")
