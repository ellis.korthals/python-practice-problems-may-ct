# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age >= 18:
        if has_consent_form:
            print("You can skydive!")
            return True
        else:
            print("Sign here, and you can skydive.")
    else:
        print("Sorry kid, you aren't even old enough to look at the form.")

can_skydive(17, False)
