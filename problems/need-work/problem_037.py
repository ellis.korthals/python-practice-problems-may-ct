# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"


# The function we want to design does the following:
#     Takes the desired length of your number, and uses the key under "pad" to extend the length
#     of your number to the selected length.

#     If the length of number is less than the length desired, add pad-key to the start of the number
#     x amount of times.

#     WE COULD ALSO DO LENGTH DESIRED MINUS THE LENGTH OF NUMBER, MULTIPLY PAD BY THAT, AND JOIN
#     THE TWO???



def pad_left(number, length, pad):
    pad_amt = int(length - len(str(number)))
    padded = str(pad_amt * pad) + str(number)

    return padded


print(pad_left(12000, 4, "*"))
