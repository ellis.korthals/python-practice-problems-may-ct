# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(a, b):
    if a > b:
        return print(f"{b} is the minimum value.")
    else:
        return print(f"{a} is the minimum value.")

minimum_value(3, 8)
