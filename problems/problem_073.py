# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.


class Student:
    def __init__(self, name):
        self.name = name
        self.scores = []

    def add_score(self, score):
        return self.scores.append(score)

    def get_average(self):
        if len(self.scores) < 1:
            return print(None)
        else:
            score_average = 0
            for i in self.scores:
                score_average = score_average + i
            return print(score_average/len(self.scores))

student = Student("Jericho")

student.add_score(12)
student.add_score(13)
student.add_score(4)

student.get_average()
