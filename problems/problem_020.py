# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    attendees_num = len(attendees_list)
    members_num = len(members_list)
    if attendees_num > members_num*0.5:
        return True
    else:
        return False

print(has_quorum(200, 80))
