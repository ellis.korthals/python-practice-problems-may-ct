# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    grade = ""
    score = 0
    for i in values:
        score = score + i
    grade = score / len(values)
    if grade >= 90:
        grade = "A"
    elif 80 <= grade < 90:
        grade = "B"
    elif 70 <= grade < 80:
        grade = "C"
    elif 60 <= grade < 70:
        grade = "D"
    else:
        grade = "F"
    return grade

print(calculate_grade([100, 90, 60, 55]))
