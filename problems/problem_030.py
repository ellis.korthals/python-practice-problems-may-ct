# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    listed_values = []
    if len(values) < 2:
        return None
    for num in values:
        listed_values.append(num)
    listed_values = sorted(listed_values)
    listed_values.reverse
    return listed_values[1]

print(find_second_largest([3, 2, 1, 4, 5]))
